package controller;

import common.service.RealCustomerService;
import model.entity.RealCustomer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/22/2020.
 */
@WebServlet("/beforeUpdate.do")
public class BeforeUpdate extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            req.setAttribute("myid",req.getParameter("id"));
            RealCustomerService realCustomerService=new RealCustomerService();
            RealCustomer realCustomer=new RealCustomer();
            realCustomer=realCustomerService.findOneById(Integer.parseInt(req.getParameter("id")));
            req.setAttribute("myName",realCustomer.getName());
            req.setAttribute("myLastName",realCustomer.getLastName());
            req.setAttribute("myIdentityNumber",realCustomer.getIdentityNumber());
            req.setAttribute("myNameOfFather",realCustomer.getNameOfFather());
            req.setAttribute("myDateOfBirth",realCustomer.getDateOfBirth());
            req.getRequestDispatcher("/edit.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
