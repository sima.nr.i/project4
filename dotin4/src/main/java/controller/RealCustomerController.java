package controller;

import common.service.RealCustomerService;
import model.entity.RealCustomer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 9/13/2020.
 */
@WebServlet("/realCustomerController.do")
public class RealCustomerController extends HttpServlet{
    private static final Logger logger = LogManager.getLogger(RealCustomerController.class);
    private RealCustomerService realCustomerService=new RealCustomerService();
    @Override
    public void service(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException {
        String action = null;
        if (req.getParameter("action")==null)
            action=(String) req.getSession().getAttribute("action");
        else
        action = req.getParameter("action");
        System.out.println("action:"+action);
        switch(action){
            case "save":
                save(req,res);
                break;
            case "findAll":
                findAll(req,res);
                break;
            case "update":
                update(req,res);
                break;
            case "delete":
                delete(req,res);
                break;
            case "search":
                search(req,res);
                break;
            case "beforeUpdate":
                beforeUpdate(req,res);
                break;

        }
}
public void save(HttpServletRequest req,HttpServletResponse resp) throws IOException {
        logger.info("METHOD SAVE START");
    RealCustomer realCustomer = new RealCustomer(req.getParameter("name"), req.getParameter("lastName")
            , req.getParameter("identityNumber"), req.getParameter("nameOfFather"), req.getParameter("dateOfBirth"));
    try {
        RealCustomerService realCustomerService=new RealCustomerService();
        realCustomerService.save(realCustomer);
        logger.info("1 REALCUSTOMER IS SAVED");
        req.setAttribute("id",realCustomer.getId());
        showCustomerNumber(req,resp);
        logger.info("ENDING OF SAVE METHOD");
    } catch (Exception e) {
        e.printStackTrace();
        resp.sendRedirect("error.jsp");
    }
}
public void search(HttpServletRequest req,HttpServletResponse resp)
{
    try {
        RealCustomer realCustomer;
        if (req.getParameter("id")=="")
        {
            realCustomer =new RealCustomer(-1,req.getParameter("name"),
                    req.getParameter("lastname"),req.getParameter("identitynumber"));

        }
        else
            realCustomer =new RealCustomer(Integer.parseInt(req.getParameter("id")),req.getParameter("name"),
                    req.getParameter("lastname"),req.getParameter("identitynumber"));
        RealCustomerService realCustomerService = new RealCustomerService();
        req.setAttribute("list", realCustomerService.search(realCustomer));
        req.getRequestDispatcher("/managingPersoncustomer.jsp").forward(req, resp);
    } catch (Exception e) {
        e.printStackTrace();
    }

}
public void findAll(HttpServletRequest request,HttpServletResponse response)
{
    try {
        RealCustomerService realCustomerService = new RealCustomerService();
        request.setAttribute("list", realCustomerService.findAll());
        request.getRequestDispatcher("/managingPersoncustomer.jsp").forward(request, response);
    } catch (Exception e) {
        e.printStackTrace();
    }
}
public void update(HttpServletRequest req, HttpServletResponse resp)
{
    RealCustomerService realCustomerService = new RealCustomerService();
    RealCustomer realCustomer = new RealCustomer(Integer.parseInt(req.getParameter("id")), req.getParameter("name"), req.getParameter("lastName")
            , req.getParameter("identityNumber"), req.getParameter("nameOfFather"), req.getParameter("dateOfBirth"));
    try {
        realCustomerService.update(realCustomer);
        findAll(req,resp);
    } catch (Exception e) {
        e.printStackTrace();
    }
}
public void delete(HttpServletRequest req,HttpServletResponse resp)
{
    try {
        RealCustomerService realCustomerService = new RealCustomerService();
        realCustomerService.delete(Integer.parseInt(req.getParameter("id")));
        findAll(req,resp);
    } catch (Exception e) {
        e.printStackTrace();
    }
}
public void beforeUpdate(HttpServletRequest req,HttpServletResponse resp)
{
    try {

        req.setAttribute("myid",req.getParameter("id"));
        RealCustomerService realCustomerService=new RealCustomerService();
        RealCustomer realCustomer=new RealCustomer();
        realCustomer=realCustomerService.findOneById(Integer.parseInt(req.getParameter("id")));
        System.out.println("realcustomer in beforeupdate"+realCustomer.getLastName());
       req.getSession().setAttribute("myname",realCustomer.getName());
       req.getSession().setAttribute("mylastname",realCustomer.getLastName());
       req.getSession().setAttribute("myidentitynumber",realCustomer.getIdentityNumber());
       req.getSession().setAttribute("mynameoffather",realCustomer.getNameOfFather());
       req.getSession().setAttribute("mydateofbirth",realCustomer.getDateOfBirth());
        req.getRequestDispatcher("/edit.jsp").forward(req, resp);
    } catch (Exception e) {
        e.printStackTrace();
    }
}
public void showCustomerNumber(HttpServletRequest req,HttpServletResponse resp)
{
    try {
        RealCustomerService realCustomerService = new RealCustomerService();
        req.setAttribute("customernumberforperson", realCustomerService.showCustomerNumber((Integer) req.getAttribute("id")));
        req.getRequestDispatcher("/showingCustomernumberForPerson.jsp").forward(req, resp);
    } catch (Exception e) {
        e.printStackTrace();
    }
}


}
