package controller;

import common.service.FacilitiesService;
import model.entity.Facilities;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 9/13/2020.
 */
@WebServlet("/facilityController.do")
public class FacilityController extends HttpServlet{
    private static final Logger logger = LogManager.getLogger(FacilityController.class);
    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String action;
        if (req.getParameter("action")==null)
            action= (String) req.getSession().getAttribute("action");
        else
            action = req.getParameter("action");
        switch (action) {
            case "save":
                save(req,res);
                break;
            case "findAll":
                findAll(req,res);
                break;
            case "beforesavefacility":
                beforeSaveFacility(req,res);
                break;
        }
    }
    public void beforeSaveFacility(HttpServletRequest req,HttpServletResponse resp) throws IOException {
        try {
            if (req.getParameter("nameOfFacility")==null ||req.getParameter("interestRate")==null||
                    req.getParameter("nameOfFacility")=="" ||req.getParameter("interestRate")=="")
            {
                resp.sendRedirect("error.jsp");
            }
            else {
                req.getSession().setAttribute("nameOfFacility", req.getParameter("nameOfFacility"));
                req.getSession().setAttribute("interestRate", req.getParameter("interestRate"));
                req.getRequestDispatcher("/awardCondition.jsp").forward(req, resp);
            }
        } catch (Exception e) {
            e.printStackTrace();
            resp.sendRedirect("error.jsp");
        }
    }
    public void save(HttpServletRequest req,HttpServletResponse resp) throws IOException {
        logger.info("SAVE METHOD FOR FACILITY IS START");
        Facilities facilities = new Facilities(req.getParameter("nameOfFacility"),req.getParameter("interestRate"),req.getParameter("name")
                ,req.getParameter("mintimeforinstruement"),req.getParameter("maxtimeforinstruement"),req.getParameter("minamountforinstruement"),
                req.getParameter("maxamountforinstruement"));
        try {
            FacilitiesService facilitiesService=new FacilitiesService();
            facilitiesService.save(facilities);
            logger.info("1 FACILITY IS SAVED");
            req.setAttribute("nameoffacility",facilities.getNameOfFacility());
            req.setAttribute("interestrate",facilities.getInterestRate());
            findAll(req,resp);
            findAll(req,resp);
            logger.info("SAVE METHOD FOR FACILITY IS END");
        } catch (Exception e) {
            e.printStackTrace();
            resp.sendRedirect("error.jsp");
        }
    }
    public void findAll(HttpServletRequest req,HttpServletResponse resp) throws IOException {
        try {
            FacilitiesService facilitiesService = new FacilitiesService();
            req.setAttribute("listofawardcondition",facilitiesService.findAll());
            req.getRequestDispatcher("/awardCondition.jsp").forward(req,resp);
        } catch (Exception e) {
            e.printStackTrace();
            resp.sendRedirect("error.jsp");
        }

    }




}
