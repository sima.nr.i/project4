package controller;

import common.service.FacilitiesService;
import common.service.FacilityAccountService;
import common.service.RealCustomerService;
import model.entity.Facilities;
import model.entity.FacilityAcount;
import model.entity.RealCustomer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Hi on 9/13/2020.
 */
@WebServlet("/facilityAccountController.do")
public class FacilityAccountController extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(FacilityAccountController.class);
    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String action;
            action = req.getParameter("action");
        switch (action) {
            case "facilityaccount":
                facilityAccount(req,res);
                break;
            case "findnamefamily":
                findNameFamily(req,res);
                break;
            case "checkcondition":
                checkCondition(req,res);
                break;
            case "facilityselectfile":
                facilitySelectFile(req,res);
                break;
        }
    }
    public void facilityAccount(HttpServletRequest req,HttpServletResponse resp) throws IOException {
         String customernumber;
        try {
            customernumber=req.getParameter("customernumber");
            req.setAttribute("customernumberforfacility",req.getParameter("customernumber"));
            findNameFamily(req,resp);
        } catch (Exception e) {
            e.printStackTrace();
            resp.sendRedirect("erro.jsp");
        }
    }
    public void findNameFamily(HttpServletRequest req,HttpServletResponse resp)
    {
        try {
            RealCustomerService realCustomerService = new RealCustomerService();
            if (req.getParameter("customernumber01")==null||req.getParameter("customernumber01")=="")
            {
                resp.sendRedirect("error.jsp");
            }
            else {
                req.getServletContext().setAttribute("customernumberforfacilityaccount", req.getParameter("customernumber01"));
                List<RealCustomer> realCustomers = realCustomerService.findNameFamily(req.getParameter("customernumber01"));
                if (realCustomers.size()==0)
                {
                    req.setAttribute("checkFindNameFamily","false");
                    req.getRequestDispatcher("error.jsp").forward(req,resp);
                    logger.info("CUSTOMERNUMBR IS WRONG,NOT SHOW NAME AND FAMILY FOR THIS CUSTOMERNUMBER");
                }
                else {
                    req.setAttribute("list03", realCustomerService.findNameFamily(req.getParameter("customernumber01")));
                    System.out.println();
                    req.getRequestDispatcher("/file.jsp").forward(req, resp);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void checkCondition(HttpServletRequest req,HttpServletResponse resp)
    {
        try {
            FacilitiesService facilitiesService = new FacilitiesService();
            System.out.println("nameoffacility"+req.getParameter("nameoffacility"));
            List<Object> facilities=facilitiesService.findMinMax(req.getParameter("nameoffacility"));
            if (facilities.size()>0)
            {
                System.out.println("notnull");
            }
            else System.out.println("null");
            String amount=req.getParameter("amount");
            String time=req.getParameter("time");
            if (amount==null||time==null||amount==""||time=="")
            {
                resp.sendRedirect("error.jsp");
            }
            else {
            List<Facilities> facilities1=new ArrayList<Facilities>();
            Iterator iterator=facilities.iterator();
            boolean check=false;
            while (iterator.hasNext())
            {
                Object[] objectList= (Object[]) iterator.next();

                System.out.println((String) objectList[0]+"::"+(String) objectList[1]+"::"+
                        (String) objectList[2]+"::"+(String) objectList[3]);
                if (Integer.parseInt(amount)<Integer.parseInt((String) objectList[0])
                        && Integer.parseInt(amount)> Integer.parseInt((String) objectList[1])
                        && Integer.parseInt(time)<Integer.parseInt((String) objectList[2])
                        && Integer.parseInt(time)>Integer.parseInt((String) objectList[3]))
                {
                    check=true;
                    FacilityAcount facilityAccount=new FacilityAcount((String)req.getServletContext().getAttribute("customernumberforfacilityaccount"),(String) req.getParameter("nameoffacility"));
                    FacilityAccountService facilityAccountService=new FacilityAccountService();
                    facilityAccountService.save(facilityAccount);
                    logger.info("1 file is saved");
                    break;
                }

            }

            if (check)
                resp.sendRedirect("/check.jsp");
            else {
                logger.info("THERE IS NO MATCHING BETWEEN CONDITION OF CUSTOMER AND CONDITION OF FACILITYFILE,SO NO FILE MAKE");
                req.setAttribute("check","conditionNotTrue");
                req.getRequestDispatcher("error.jsp").forward(req,resp);

            }}

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void facilitySelectFile(HttpServletRequest req,HttpServletResponse resp)
    {

        try {
            FacilitiesService facilitiesService = new FacilitiesService();

            List<Object> facilities=facilitiesService.findAllNoReapeat();
            List<Facilities> facilities1=new ArrayList<Facilities>();
            Iterator iterator=facilities.iterator();
            while (iterator.hasNext())
            {
                Object[] objectList= (Object[]) iterator.next();
                System.out.println("in select file"+(String) objectList[0]+(String) objectList[1]);
                Facilities facilities2=new Facilities();
                facilities2.setInterestRate((String) objectList[1]);
                facilities2.setNameOfFacility((String) objectList[0]);
                facilities1.add(facilities2);

            }
            req.getSession().setAttribute("list05", facilities1);
            req.getRequestDispatcher("/file.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
