package controller;

import common.service.LegalCustomerService;
import model.entity.LegalCustomer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hi on 9/12/2020.
 */
@WebServlet("/legalCustomerController.do")
public class LegalCustomerController extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(LegalCustomerController.class);
    private LegalCustomerService legalCustomerService=new LegalCustomerService();

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String action;
        if (req.getParameter("action")==null)
            action= (String) req.getSession().getAttribute("action");
        else
        action = req.getParameter("action");
        switch (action) {
            case "save":
                save(req,res);
                break;
            case "findAll":
                findAll(req,res);
                break;
            case "update":
                update(req,res);
                break;
            case "delete":
                delete(req,res);
                break;
            case "search":
               search(req,res);
                break;
            case "beforeUpdate":
                beforeupdate(req,res);
        }
    }
    public void save(HttpServletRequest req,HttpServletResponse resp) throws IOException {
        logger.info("SAVE METHOD FOR LEGAL CUSTOMER IS START");
        LegalCustomer legalCustomer = new LegalCustomer(req.getParameter("companyName"), req.getParameter("economicId")
                , req.getParameter("dateOfRegistering"));
        try {
            LegalCustomerService legalCustomerService = new LegalCustomerService();
            legalCustomerService.save(legalCustomer);
            logger.info("1 LEGAL CUSTOMER IS SAVED");
            req.setAttribute("id2",legalCustomer.getId());
            showCustomerNumber(req,resp);
            logger.info("SAVE METHOD FOR LEGAL CUSTOMER IS ENDING");
        } catch (Exception e) {
            e.printStackTrace();
            resp.sendRedirect("error.jsp");
        }

    }
    public void showCustomerNumber(HttpServletRequest req,HttpServletResponse resp)
    {
        try {
            LegalCustomerService legalCustomerService = new LegalCustomerService();
            req.setAttribute("customernumberforcompany", legalCustomerService.showCustomerNumber((Integer) req.getAttribute("id2")));
            req.getRequestDispatcher("/showingCustomernumberForCompany.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void findAll(HttpServletRequest req,HttpServletResponse resp)
    {
        try {
            LegalCustomerService legalCustomerService = new LegalCustomerService();
            req.setAttribute("list2", legalCustomerService.findAll());
            req.getRequestDispatcher("/managingCompanycustomer.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void beforeupdate(HttpServletRequest req,HttpServletResponse resp)
    {
        try {
            req.setAttribute("myid1",req.getParameter("id3"));
            req.getRequestDispatcher("/edit.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void update(HttpServletRequest req,HttpServletResponse resp)
    {
        LegalCustomerService legalCustomerService = new LegalCustomerService();
        LegalCustomer legalCustomer = new LegalCustomer(Integer.parseInt(req.getParameter("companyId")), req.getParameter("companyName"), req.getParameter("economicId")
                , req.getParameter("dateOfRegistering"));
        try {
            System.out.println("in service update:"+"id:"+legalCustomer.getId()+"company name"+legalCustomer.getCompanyName()+"economicid"+legalCustomer.getEconomicId());
            legalCustomerService.update(legalCustomer);
            findAll(req,resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(HttpServletRequest req,HttpServletResponse resp)
    {
        try {
            LegalCustomerService legalCustomerService = new LegalCustomerService();
            legalCustomerService.delete(Integer.parseInt(req.getParameter("id")));
            findAll(req,resp);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void search(HttpServletRequest req,HttpServletResponse resp)
    {
        try {
            LegalCustomer legalCustomer;
            if (req.getParameter("id")==""||req.getParameter("id")==null)
            {
                legalCustomer =new LegalCustomer(-1,req.getParameter("companyname"),
                        req.getParameter("economicid"));
            }
            else
                legalCustomer =new LegalCustomer(Integer.parseInt(req.getParameter("id")),req.getParameter("companyname"),
                        req.getParameter("economicid"));
            LegalCustomerService legalCustomerService = new LegalCustomerService();
            List<LegalCustomer> legalCustomers =new ArrayList<LegalCustomer>();
            legalCustomers = legalCustomerService.search(legalCustomer);
            req.setAttribute("list2", legalCustomerService.search(legalCustomer));
            req.getRequestDispatcher("/managingCompanycustomer.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    }

