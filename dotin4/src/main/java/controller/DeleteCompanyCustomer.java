package controller;

import common.service.LegalCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/3/2020.
 */
@WebServlet("/deleteCompanyCustomer.do")
public class DeleteCompanyCustomer extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            LegalCustomerService legalCustomerService = new LegalCustomerService();
            legalCustomerService.delete(Integer.parseInt(req.getParameter("id")));
            req.getSession().setAttribute("action","findAll");
            req.getRequestDispatcher("/legalCustomerController.do").forward(req,resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
