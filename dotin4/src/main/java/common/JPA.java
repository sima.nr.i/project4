package common;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by Bahador on 2/23/2019.
 */
public class JPA {
    private static EntityManagerFactory factory;
    static {
        factory = Persistence.createEntityManagerFactory("X");
    }
    public static EntityManager getManager()
    {
        return factory.createEntityManager();
    }
    public static void init()
    {
        factory = Persistence.createEntityManagerFactory("X");
    }
}
