package common.service;

import com.sun.org.apache.regexp.internal.RE;
import common.JPA;
import model.entity.RealCustomer;
import common.repository.PersonCustomerDA;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.jpamodelgen.xml.jaxb.Entity;
import org.hibernate.jpamodelgen.xml.jaxb.Persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by Hi on 8/1/2020.
 */
public class RealCustomerService {
    public int countCustomers=0;
    public  List<Integer> customernumbers = new ArrayList<Integer>();
    public  void save(RealCustomer realCustomer) throws Exception {
//        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
//        personCustomerDA.insert(person);
//        personCustomerDA.close();
        countCustomers++;
        for (int i = 100; i < 1000; i++) {
            customernumbers.add(new Integer(i));
        }
        Collections.shuffle(customernumbers);
        EntityManager entityManager= JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        realCustomer.setCustomerNumber(String.valueOf((customernumbers.get(countCustomers))));
        entityManager.persist(realCustomer);
        entityTransaction.commit();
        entityManager.close();
    }

    public  String showCustomerNumber(int id)throws Exception
    {
//        PersonCustomerDA personCustomerDA=new PersonCustomerDA();
//        String customerNumber=personCustomerDA.selectCustomerNumber();
//        personCustomerDA.close();
//        return customerNumber;
        EntityManager entityManager=JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        RealCustomer realCustomer02=entityManager.find(RealCustomer.class,id);
        String customerNumber=realCustomer02.getCustomerNumber();
        entityTransaction.commit();
        entityManager.close();
        return customerNumber;

    }
    public  List<RealCustomer> findAll()throws Exception
    {
//        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
//        List<RealCustomer> personList = personCustomerDA.select();
//        personCustomerDA.close();
//        return personList;

        //FIND ALL BY JPQL
        EntityManager entityManager=JPA.getManager();
        Query query=entityManager.createQuery("select p from realcustomer p");
        List<RealCustomer> realCustomers=query.getResultList();
        entityManager.close();
        return realCustomers;
    }

    public  List<RealCustomer> search(RealCustomer realCustomer)throws Exception
    {
//        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
//        List<RealCustomer> personList = personCustomerDA.search(realCustomer);
//        personCustomerDA.close();
//        return personList;
        EntityManager entityManager=JPA.getManager();
        Query  query=entityManager.createQuery("select p from realcustomer p where" +
                "(1=:a or p.id=:b) AND (1=:c or p.name=:d)AND(1=:e or p.lastName=:f) AND(1=:g or p.identityNumber=:h)");
        if (realCustomer.getId()==-1) {
            query.setParameter("a",1);

        }
        else {
            query.setParameter("a",0);
        }
        query.setParameter("b", realCustomer.getId());
        if (realCustomer.getName()==null|| realCustomer.getName()=="")
        {
            query.setParameter("c",1);
        }
        else query.setParameter("c",0);
        query.setParameter("d",realCustomer.getName());
        if (realCustomer.getLastName()==null|| realCustomer.getLastName()=="")
        {
            query.setParameter("e",1);
        }
        else query.setParameter("e",0);
        query.setParameter("f", realCustomer.getLastName());
        if (realCustomer.getIdentityNumber()==null|| realCustomer.getIdentityNumber()=="")
        {
            query.setParameter("g",1);
        }
        else query.setParameter("g",0);
        query.setParameter("h", realCustomer.getIdentityNumber());
        List<RealCustomer> realCustomers=query.getResultList();
        entityManager.close();
        return realCustomers;
    }

    public void update(RealCustomer realCustomer) throws Exception {
//        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
//        personCustomerDA.update(person);
//        personCustomerDA.close();
        EntityManager entityManager=JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        RealCustomer realCustomer1=entityManager.find(RealCustomer.class,realCustomer.getId());
        if (realCustomer.getName()!="")
            realCustomer1.setName(realCustomer.getName());
        if (realCustomer.getLastName()!="")
            realCustomer1.setLastName(realCustomer.getLastName());
        if (realCustomer.getIdentityNumber()!="")
            realCustomer1.setIdentityNumber(realCustomer.getIdentityNumber());
        if (realCustomer.getNameOfFather()!="")
            realCustomer1.setNameOfFather(realCustomer.getNameOfFather());
        if (realCustomer.getDateOfBirth()!="")
            realCustomer1.setDateOfBirth(realCustomer.getDateOfBirth());
        entityManager.persist(realCustomer1);
        entityTransaction.commit();
        entityManager.close();

    }
    public static void delete(int id) throws Exception {
//        PersonCustomerDA personCustomerDA = new PersonCustomerDA();
//        personCustomerDA.delete(id);
//        personCustomerDA.close();
        EntityManager entityManager=JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        RealCustomer realCustomer=entityManager.find(RealCustomer.class,id);
        entityManager.remove(realCustomer);
        entityTransaction.commit();
        entityManager.close();
    }
    public List<RealCustomer> findNameFamily(String customerNumber)
    {
        EntityManager entityManager=JPA.getManager();
        System.out.println("in real customer service:"+customerNumber);
        Query query=entityManager.createQuery("select p from realcustomer p where p.customerNumber=:x");
        query.setParameter("x",customerNumber);
        List<RealCustomer> realCustomers=query.getResultList();
        entityManager.close();
        return realCustomers;
    }
    public RealCustomer findOneById(int id)
    {
        EntityManager entityManager=JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        RealCustomer realCustomer=entityManager.find(RealCustomer.class,id);
        entityTransaction.commit();
        entityManager.close();
        return realCustomer;
    }


}
