package common.service;

import common.JPA;
import model.entity.FacilityAcount;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * Created by Hi on 9/12/2020.
 */
public class FacilityAccountService  {

    public void save(FacilityAcount facilityAcount)
    {
        EntityManager entityManager= JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        entityManager.persist(facilityAcount);
        entityTransaction.commit();
        entityManager.close();
    }
}
