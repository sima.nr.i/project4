package common.service;

import common.JPA;
import model.entity.LegalCustomer;
import common.repository.CompanyCustomerDA;
import model.entity.RealCustomer;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Hi on 8/1/2020.
 */
public class LegalCustomerService {
    private  int countCustomers=0;
    private List<Integer> customernumbers = new ArrayList<Integer>();
    public void save(LegalCustomer legalCustomer) throws Exception {
//        CompanyCustomerDA companyCustomerDA = new CompanyCustomerDA();
//        companyCustomerDA.insert(legalCustomer);
//        companyCustomerDA.close();
        countCustomers++;
        for (int i = 100; i < 1000; i++) {
            customernumbers.add(new Integer(i));
        }
        Collections.shuffle(customernumbers);
        EntityManager entityManager= JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        legalCustomer.setCustomerNumber(String.valueOf((customernumbers.get(countCustomers))));
        entityManager.persist(legalCustomer);
        entityTransaction.commit();
        entityManager.close();
    }

    public String showCustomerNumber(int id) throws Exception {
//        CompanyCustomerDA companyCustomerDA = new CompanyCustomerDA();
//        String customerNumber = companyCustomerDA.selectCustomerNumber();
//        companyCustomerDA.close();
//        return customerNumber;
        EntityManager entityManager=JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        LegalCustomer legalCustomer=entityManager.find(LegalCustomer.class,id);
        String customerNumber=legalCustomer.getCustomerNumber();
        entityTransaction.commit();
        entityManager.close();
        return customerNumber;
    }

    public static List<LegalCustomer> findAll() throws Exception {
//        CompanyCustomerDA companyCustomerDA = new CompanyCustomerDA();
//        List<LegalCustomer> legalCustomers = companyCustomerDA.select();
//        companyCustomerDA.close();
//        return legalCustomers;
        //FINDALLBYJPAQL
        EntityManager entityManager=JPA.getManager();
        Query query=entityManager.createQuery("select p from legalcustomer p");
        List<LegalCustomer> legalCustomers=query.getResultList();
        entityManager.close();
        return legalCustomers;
    }


    public  List<LegalCustomer> search(LegalCustomer legalCustomer) throws Exception {
//        CompanyCustomerDA companyCustomerDA = new CompanyCustomerDA();
//        List<LegalCustomer> legalCustomers = companyCustomerDA.search(legalCustomer);
//        companyCustomerDA.close();
//        return legalCustomers;
        EntityManager entityManager=JPA.getManager();
        Query  query=entityManager.createQuery("select p from legalcustomer p where" +
                "(1=:a or p.id=:b) AND (1=:c or p.companyName=:d)AND(1=:e or p.economicId=:f) AND(1=:g or p.dateOfRegistering=:h)");
        if (legalCustomer.getId()==-1) {
            query.setParameter("a",1);

        }
        else {
            query.setParameter("a",0);
        }
        query.setParameter("b", legalCustomer.getId());
        if (legalCustomer.getCompanyName()==null|| legalCustomer.getCompanyName()=="")
        {
            query.setParameter("c",1);
        }
        else query.setParameter("c",0);
        query.setParameter("d",legalCustomer.getCompanyName());
        if (legalCustomer.getEconomicId()==null|| legalCustomer.getEconomicId()=="")
        {
            query.setParameter("e",1);
        }
        else query.setParameter("e",0);
        query.setParameter("f", legalCustomer.getEconomicId());
        if (legalCustomer.getDateOfRegistering()==null|| legalCustomer.getDateOfRegistering()=="")
        {
            query.setParameter("g",1);
        }
        else query.setParameter("g",0);
        query.setParameter("h", legalCustomer.getDateOfRegistering());
        List<LegalCustomer> legalCustomers=query.getResultList();
        return legalCustomers;
    }
    public void update(LegalCustomer legalCustomer) throws Exception {
//        CompanyCustomerDA companyCustomerDA = new CompanyCustomerDA();
//        companyCustomerDA.update(legalCustomer);
//        companyCustomerDA.close();
        EntityManager entityManager=JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        LegalCustomer legalCustomer1=entityManager.find(LegalCustomer.class,legalCustomer.getId());
        if (legalCustomer.getCompanyName()!="")
            legalCustomer1.setCompanyName(legalCustomer.getCompanyName());
        if (legalCustomer.getEconomicId()!="")
            legalCustomer1.setEconomicId(legalCustomer.getEconomicId());
        if (legalCustomer.getDateOfRegistering()!="")
            legalCustomer1.setDateOfRegistering(legalCustomer.getDateOfRegistering());
        System.out.println("in legal service"+"company name"+legalCustomer1.getCompanyName()+"economic id"+legalCustomer1.getEconomicId()
        +"date of registering"+legalCustomer1.getDateOfRegistering());
        entityManager.persist(legalCustomer1);
        entityTransaction.commit();
        entityManager.close();
    }

    public  void delete(int id) throws Exception {
//        CompanyCustomerDA companyCustomerDA = new CompanyCustomerDA();
//        companyCustomerDA.delete(id);
//        companyCustomerDA.close();

        EntityManager entityManager=JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        LegalCustomer legalCustomer=entityManager.find(LegalCustomer.class,id);
        entityManager.remove(legalCustomer);
        entityTransaction.commit();
        entityManager.close();
    }
    public LegalCustomer findOneById(int id)
    {
        EntityManager entityManager=JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        LegalCustomer legalCustomer=entityManager.find(LegalCustomer.class,id);
        entityTransaction.commit();
        entityManager.close();
        return legalCustomer;
    }
}
