package common.service;

import common.JPA;
import model.entity.Facilities;
import oracle.jdbc.OracleTypeMetaData;
import org.hibernate.jpamodelgen.xml.jaxb.Entity;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Hi on 9/1/2020.
 */
public class FacilitiesService {
    public void save(Facilities facility){
        EntityManager entityManager= JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        entityManager.persist(facility);
        entityTransaction.commit();
        entityManager.close();
    }
    public void saveFacility(Facilities facilities)
    {
        EntityManager entityManager=JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        entityManager.persist(facilities);
        entityTransaction.commit();
        entityManager.close();
    }
    public void saveAwardcondition(Facilities facilities)
    {
        EntityManager entityManager=JPA.getManager();
        EntityTransaction entityTransaction=entityManager.getTransaction();
        entityTransaction.begin();
        entityManager.persist(facilities);
        entityTransaction.commit();
        entityManager.close();
    }

    public List<Facilities> findAll()
    {
        EntityManager entityManager=JPA.getManager();
        Query query=entityManager.createQuery("select p from facilities p");
        List<Facilities> facilities=query.getResultList();
        return facilities;
    }
    public List<Object> findAllNoReapeat()
    {
        EntityManager entityManager=JPA.getManager();
      Query query=entityManager.createNamedQuery("x3");
        List<Object>  facilities=query.getResultList();
        return facilities;
    }
    public List<Object> findMinMax(String nameOfFacility)
    {
        EntityManager entityManager=JPA.getManager();
        Query query=entityManager.createNamedQuery("query2");
        query.setParameter("i",nameOfFacility);
        List<Object> objectList=query.getResultList();
        return objectList;


    }

}
