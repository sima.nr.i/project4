package model.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Hi on 9/1/2020.
 */
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
@Entity(name ="facilities")
@Table(name = "FACILITIES")

@NamedNativeQueries(
        {@NamedNativeQuery(name = "x3", query = "select DISTINCT nameoffacility,interestrate from facilities"),
        @NamedNativeQuery(name = "query2",query = " select maxamountforinstruement,minamountforinstruement," +
                "maxtimeforinstruement,mintimeforinstruement from facilities where nameoffacility=:i")
        }
)

public class Facilities implements Serializable {
    @Id
    @Column(columnDefinition = "NUMBER",nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(columnDefinition = "VARCHAR(20)",nullable = false)
    private String nameOfFacility;
    @Column(columnDefinition = "VARCHAR(20)",nullable = false)
    private String interestRate;
    @Column(columnDefinition = "VARCHAR(20)",nullable = false)
    private String nameOfCondition;
    @Column(columnDefinition = "VARCHAR(20)",nullable = false)
    private String minTimeForInstruement;
    @Column(columnDefinition = "VARCHAR(20)",nullable = false)
    private String maxTimeForInstruement;
    @Column(columnDefinition = "VARCHAR(20)",nullable = false)
    private String minAmountForInstruement;
    @Column(columnDefinition = "VARCHAR(20)",nullable = false)
    private String maxAmountForInstruement;

    public String getNameOfCondition() {
        return nameOfCondition;
    }

    public Facilities(String nameOfFacility, String interestRate, String nameOfCondition, String minTimeForInstruement, String maxTimeForInstruement, String minAmountForInstruement, String maxAmountForInstruement) {
        this.nameOfFacility = nameOfFacility;
        this.interestRate = interestRate;
        this.nameOfCondition = nameOfCondition;
        this.minTimeForInstruement = minTimeForInstruement;
        this.maxTimeForInstruement = maxTimeForInstruement;
        this.minAmountForInstruement = minAmountForInstruement;
        this.maxAmountForInstruement = maxAmountForInstruement;
    }

    public Facilities(String nameOfFacility, String interestRate) {
        this.nameOfFacility = nameOfFacility;
        this.interestRate = interestRate;
    }

    public Facilities(String nameOfCondition, String minTimeForInstruement, String maxTimeForInstruement, String minAmountForInstruement, String maxAmountForInstruement) {
        this.nameOfCondition = nameOfCondition;
        this.minTimeForInstruement = minTimeForInstruement;
        this.maxTimeForInstruement = maxTimeForInstruement;
        this.minAmountForInstruement = minAmountForInstruement;
        this.maxAmountForInstruement = maxAmountForInstruement;
    }

    public Facilities(){}

    public void setNameOfCondition(String nameOfCondition) {
        this.nameOfCondition = nameOfCondition;
    }

    public String getMinTimeForInstruement() {
        return minTimeForInstruement;
    }

    public void setMinTimeForInstruement(String minTimeForInstruement) {
        this.minTimeForInstruement = minTimeForInstruement;
    }

    public String getMaxTimeForInstruement() {
        return maxTimeForInstruement;
    }

    public void setMaxTimeForInstruement(String maxTimeForInstruement) {
        this.maxTimeForInstruement = maxTimeForInstruement;
    }

    public String getMinAmountForInstruement() {
        return minAmountForInstruement;
    }

    public void setMinAmountForInstruement(String minAmountForInstruement) {
        this.minAmountForInstruement = minAmountForInstruement;
    }

    public String getMaxAmountForInstruement() {
        return maxAmountForInstruement;
    }

    public void setMaxAmountForInstruement(String maxAmountForInstruement) {
        this.maxAmountForInstruement = maxAmountForInstruement;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameOfFacility() {
        return nameOfFacility;
    }

    public void setNameOfFacility(String nameOfFacility) {
        this.nameOfFacility = nameOfFacility;
    }

    public String getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }
}
