<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="model.entity.RealCustomer" %>
<%@ page import="java.util.List" %>
<%@ page import="model.entity.Facilities" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="javafx.application.Application" %><%--
  Created by IntelliJ IDEA.
  User: Hi
  Date: 9/5/2020
  Time: 12:01 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <title>Title</title>
    <style>

        {box-sizing: border-box}
        .container {
            position: relative;
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px 0 30px 0;
        }

        input,
        .btn {

            width: 100%;
            padding: 12px;
            border: none;
            border-radius: 4px;
            margin: 5px 0;
            opacity: 0.85;
            display: inline-block;
            font-size: 17px;
            line-height: 20px;
            text-decoration: none; /* remove underline from anchors */
        }

        input:hover,
        .btn:hover {
            opacity: 1;
        }
        .fb {

            /*float: none;*/
            /*position: absolute;*/
            /*top: 50%;*/
            /*left: 50%;*/
            /*transform: translate(-50%, -50%);*/
            text-align: center;
            background-color: #3B5998;
            color: white;
        }

        .twitter {

            /*float: none;*/
            /*position: absolute;*/
            /*top: 50%;*/
            /*left: 50%;*/
            /*transform: translate(-50%, -50%);*/

            text-align: center;
            background-color: #55ACEE;
            color: white;
        }

        /* style the submit button */
        input[type=submit] {
            background-color: #4CAF50;
            color: white;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #45a049;
        }

        /* Two-column layout */
        .col {
            float: left;
            width: 50%;
            margin: auto;
            padding: 0 50px;
            margin-top: 6px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* vertical line */
        .vl {
            position: absolute;
            left: 50%;
            transform: translate(-50%);
            border: 2px solid #ddd;
            height: 175px;
        }

        /* text inside the vertical line */
        .inner {
            position: absolute;
            top: 50%;
            transform: translate(-50%, -50%);
            background-color: #f1f1f1;
            border: 1px solid #ccc;
            border-radius: 50%;
            padding: 8px 10px;
        }

        /* hide some text on medium and large screens */
        .hide-md-lg {
            display: none;
        }

        /* bottom container */
        .bottom-container {
            text-align: center;
            background-color: #666;
            border-radius: 0px 0px 4px 4px;
        }

        /* Responsive layout - when the screen is less than 650px wide, make the two columns stack on top of each other instead of next to each other */
        @media screen and (max-width: 650px) {
            .col {
                width: 100%;
                margin-top: 0;
            }
            /* hide the vertical line */
            .vl {
                display: none;
            }
            /* show the hidden text on small screens */
            .hide-md-lg {
                display: block;
                text-align: center;
            }
        }






        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }



        #customers2 {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers2 td, #customers2 th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers2 tr:nth-child(even){background-color: #f2f2f2;}

        #customers2 tr:hover {background-color: #ddd;}

        #customers2 th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }





        input[type=number], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }


        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
        /* Style the submit button */
        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        /* Add a background color to the submit button on mouse-over */
        input[type=submit]:hover {
            background-color: #45a049;
        }



        .hideHeader {
            display: none;
        }

    </style>
</head>
<body>

<form action="/facilityAccountController.do">
    <input type="hidden" name="action" value="findnamefamily">
    <input type="text" name="customernumber01" id="customernumber01">
    <input type="submit" value="بازیابی">

</form>
<script>
    document.getElementById("customernumber01").value = localStorage.getItem("comment");
    function saveComment() {
        var comment = document.getElementById("customernumber01").value;
        if (comment == "") {
            alert("Please enter a comment in first!");
            return false;
        }
        localStorage.setItem("comment", comment);
        alert("Your comment has been saved!");

        location.reload();
        return false;

    }
</script>
<table border="0" style="width: 100%" id="customers">
    <tr>
        <th>NAME</th>
        <th>LASTNAME</th>
    </tr>
    <c:forEach items="${requestScope.list03}" var="p">
        <tr>
            <td><c:out value="${p.name}"/></td>
            <td><c:out value="${p.lastName}"/></td>
        </tr>
    </c:forEach>
</table>

<%--<form action="/facilityAccountController.do">--%>
    <%--<input type="hidden" name="action" value="checkcondition">--%>
    <%--<input type="text" name="amount" placeholder="AMOUNT">--%>
    <%--<input type="text" name="time" placeholder="TIME">--%>
<%--<table border="0" style="width: 100%" id="customers2">--%>
    <%--<tr>--%>
        <%--<th>NAMEOFFACILITY</th>--%>
        <%--<th>INTERESTRATE</th>--%>
    <%--</tr>--%>

    <%--<c:forEach items="${sessionScope.list05}" var="p">--%>
        <%--<tr>--%>
            <%--<td><input readonly type="text" name="nameoffacility" value="<c:out value="${p.nameOfFacility}"/>" placeholder="<c:out value="${p.nameOfFacility}"/>"></td>--%>
            <%--<%request.setAttribute("nameoffacility02",request.getParameter("nameoffacility"));%>--%>
            <%--<td><input readonly type="text" name="interestrate" value="<c:out value="${p.interestRate}"/>" placeholder="<c:out value="${p.interestRate}"/>" /></td>--%>
            <%--<td><input type="submit" value="چک شود"></td>--%>
        <%--</tr>--%>
    <%--</c:forEach>--%>
<%--</table>--%>

<form action="/facilityAccountController.do">
    <input type="hidden" name="action" value="checkcondition">
    <input type="text" name="amount" placeholder="AMOUNT">
    <input type="text" name="time" placeholder="TIME">

    <select id="nameoffacility" name="nameoffacility">
        <option value="0">نوع تسهیلات:</option>
        <c:forEach items="${sessionScope.list05}" var="p">
        <option  value="<c:out value="${p.nameOfFacility}"/>"><c:out value="${p.nameOfFacility}"/></option>
        </c:forEach>
        </select>
    <input type="submit" value="چک و ثبت شود">
</form>
</body>
</html>