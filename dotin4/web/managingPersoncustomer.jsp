<%@ page import="common.repository.PersonCustomerDA" %>
<%@ page import="javafx.application.Application" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Hi
  Date: 8/5/2020
  Time: 8:56 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style>

        {box-sizing: border-box}
        .container {
            position: relative;
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px 0 30px 0;
        }

        input,
        .btn {

            width: 100%;
            padding: 12px;
            border: none;
            border-radius: 4px;
            margin: 5px 0;
            opacity: 0.85;
            display: inline-block;
            font-size: 17px;
            line-height: 20px;
            text-decoration: none; /* remove underline from anchors */
        }

        input:hover,
        .btn:hover {
            opacity: 1;
        }
        .fb {

            /*float: none;*/
            /*position: absolute;*/
            /*top: 50%;*/
            /*left: 50%;*/
            /*transform: translate(-50%, -50%);*/
            text-align: center;
            background-color: #3B5998;
            color: white;
        }

        .twitter {

            /*float: none;*/
            /*position: absolute;*/
            /*top: 50%;*/
            /*left: 50%;*/
            /*transform: translate(-50%, -50%);*/

            text-align: center;
            background-color: #55ACEE;
            color: white;
        }

        /* style the submit button */
        input[type=submit] {
            background-color: #4CAF50;
            color: white;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #45a049;
        }

        /* Two-column layout */
        .col {
            float: left;
            width: 50%;
            margin: auto;
            padding: 0 50px;
            margin-top: 6px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* vertical line */
        .vl {
            position: absolute;
            left: 50%;
            transform: translate(-50%);
            border: 2px solid #ddd;
            height: 175px;
        }

        /* text inside the vertical line */
        .inner {
            position: absolute;
            top: 50%;
            transform: translate(-50%, -50%);
            background-color: #f1f1f1;
            border: 1px solid #ccc;
            border-radius: 50%;
            padding: 8px 10px;
        }

        /* hide some text on medium and large screens */
        .hide-md-lg {
            display: none;
        }

        /* bottom container */
        .bottom-container {
            text-align: center;
            background-color: #666;
            border-radius: 0px 0px 4px 4px;
        }

        /* Responsive layout - when the screen is less than 650px wide, make the two columns stack on top of each other instead of next to each other */
        @media screen and (max-width: 650px) {
            .col {
                width: 100%;
                margin-top: 0;
            }
            /* hide the vertical line */
            .vl {
                display: none;
            }
            /* show the hidden text on small screens */
            .hide-md-lg {
                display: block;
                text-align: center;
            }
        }

        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }
        input[type=number], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }


        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
        /* Style the submit button */
        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        /* Add a background color to the submit button on mouse-over */
        input[type=submit]:hover {
            background-color: #45a049;
        }

        .hideHeader {
            display: none;
        }

    </style>
    <title>Title</title>
    <script>
        function alphaOnly(event) {
            var key = event.keyCode;
            return ((key >= 65 && key <= 90) || key == 8);
        }
    </script>
    <script>
        function updaterealcustomer(Id) {
            console.log("updaterealcustomer");
            localStorage.setItem("idforrealcustomer",Id);
            if (confirm("are you sure?"))
     window.location = '/beforeUpdate.do?id=' + Id;
        }
    </script>
    <script>
        function deleterealcustomer(Id) {
            if (confirm("are you sure?"))
                window.location = '/delete.do?id=' + Id;
        }
    </script>
</head>
<body>
<form action="/realCustomerController.do">
    <input hidden type="hidden" name="action" value="search">
    <input type="number" name="id" placeholder="ID" >
    <input type="text" name="name" placeholder="NAME"onkeydown="return alphaOnly(event)">
    <input type="text" name="lastname" placeholder="LAST NAME" onkeydown="return alphaOnly(event)">
    <input type="number" name="identitynumber" placeholder="IDENTITY NUMBER" >
    <input type="submit" value="جستجو">
</form>

<form action="insertPersoncustomer.jsp">
    <input type="submit" value="تعریف مشتری">
</form>
<form action="/facility.jsp">
    <input type="submit" value="نوع تسهیلات">
</form>
<form action="/facilityAccountController.do">
    <input type="hidden" name="action" value="facilityselectfile">
    <input type="submit" value="پرونده تسهیلاتی">
</form>
<table border="0" style="width: 100%" id="customers">
    <tr>
        <th>ID</th>
        <th>CUSTOMERNUMBER</th>
        <th>NAME</th>
        <th>FAMILY</th>
        <th>identityNumber</th>
        <th>nameOfFather</th>
        <th>dateOfBirth</th>
    </tr>
    <c:forEach items="${requestScope.list}" var="p">
        <tr>
            <td><c:out value="${p.id}"/></td>
            <td><c:out value="${p.customerNumber}"/></td>
            <td><c:out value="${p.name}"/></td>
            <td><c:out value="${p.lastName}"/></td>
            <td><c:out value="${p.identityNumber}"/></td>
            <td><c:out value="${p.nameOfFather}"/></td>
            <td><c:out value="${p.dateOfBirth}"/></td>
            <td><input type="button" onclick="deleterealcustomer(${p.id})" value="حذف"/></td>
            <td><input type="button" onclick="updaterealcustomer(${p.id})" value="ویرایش"/></td>

        </tr>
    </c:forEach>
</table>
</body>
</html>
