package model.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Facilities.class)
public abstract class Facilities_ {

	public static volatile SingularAttribute<Facilities, String> interestRate;
	public static volatile SingularAttribute<Facilities, String> maxTimeForInstruement;
	public static volatile SingularAttribute<Facilities, String> nameOfFacility;
	public static volatile SingularAttribute<Facilities, String> maxAmountForInstruement;
	public static volatile SingularAttribute<Facilities, String> minAmountForInstruement;
	public static volatile SingularAttribute<Facilities, String> minTimeForInstruement;
	public static volatile SingularAttribute<Facilities, Integer> id;
	public static volatile SingularAttribute<Facilities, String> nameOfCondition;

}

