package model.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(LegalCustomer.class)
public abstract class LegalCustomer_ {

	public static volatile SingularAttribute<LegalCustomer, String> companyName;
	public static volatile SingularAttribute<LegalCustomer, Integer> id;
	public static volatile SingularAttribute<LegalCustomer, String> economicId;
	public static volatile SingularAttribute<LegalCustomer, String> customerNumber;
	public static volatile SingularAttribute<LegalCustomer, String> dateOfRegistering;

}

