package model.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FacilityAcount.class)
public abstract class FacilityAcount_ {

	public static volatile SingularAttribute<FacilityAcount, String> facilityAccountType;
	public static volatile SingularAttribute<FacilityAcount, Integer> id;
	public static volatile SingularAttribute<FacilityAcount, String> customerNumber;

}

